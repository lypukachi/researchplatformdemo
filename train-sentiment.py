
import os
import warnings
import sys
import time
import string
import pandas as pd
import numpy as np

from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score, accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.linear_model import ElasticNet
from sklearn.feature_extraction.text import TfidfVectorizer
from urllib.parse import urlparse
import mlflow
import mlflow.sklearn

import logging

logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)


def eval_metrics(actual, pred):
    rmse = np.sqrt(mean_squared_error(actual, pred))
    mae = mean_absolute_error(actual, pred)
    r2 = r2_score(actual, pred)
    return rmse, mae, r2

def clear_sentence(sentence: str) -> str:
    sentence = sentence.translate(str.maketrans(string.punctuation, ' ' * len(string.punctuation)))
    sentence = sentence.lower()
    return sentence

if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    np.random.seed(40)

    # Read the wine-quality csv file from the URL
    csv_url = ("training.1600000.processed.noemoticon.csv")
    try:
        data = pd.read_csv(csv_url, encoding="ISO-8859-1", names=["target", "ids", "date", "flag", "user", "text"],sep=",")
    except Exception as e:
        logger.exception(
            "Unable to download training & test CSV, check your internet connection. Error: %s", e
        )

    # Now, we won't be using any other data other than the text and the sentiment. 
    data = data[['target','text']]

    # Make the sentiments strings
    sentiment_value = {0: "negative", 2: "neutral", 4: "positive"}
    decode = lambda label: sentiment_value[int(label)]
    x = data['text'].apply(clear_sentence).tolist()
    y = data['target'].apply(decode).tolist()

    # Let's use the SVC model we used before.
    starting_time = time.time()   
    vector = TfidfVectorizer(ngram_range=(1, 2))
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
    X_training = vector.fit_transform(X_train) 
    X_testing = vector.transform(X_test)
   

    alpha = float(sys.argv[1]) if len(sys.argv) > 1 else 0.5
    l1_ratio = float(sys.argv[2]) if len(sys.argv) > 2 else 0.5

    with mlflow.start_run():
        model = ElasticNet(alpha=alpha, l1_ratio=l1_ratio, random_state=42)
        print('train model...')
        model.fit(X_training, y_train)

        y_prediction = model.predict(X_testing)
       
        accuracy = accuracy_score(y_test, y_prediction)
        (rmse, mae, r2) = eval_metrics(y_test, y_prediction)
        ending_time = time.time()
        
        print('Trained our model in',len(Sentiments.index),'tweets')
        print('Accuracy:',"{:.2f}".format(accuracy*100) + " in {:.2f}s".format(ending_time-starting_time))
        
        print("Elasticnet model (alpha=%f, l1_ratio=%f):" % (alpha, l1_ratio))
        print("  RMSE: %s" % rmse)
        print("  MAE: %s" % mae)
        print("  R2: %s" % r2)

        mlflow.log_param("alpha", alpha)
        mlflow.log_param("l1_ratio", l1_ratio)
        mlflow.log_metric("rmse", rmse)
        mlflow.log_metric("r2", r2)
        mlflow.log_metric("mae", mae)
        
        #logging metric to mlflow
        mlflow.log_metric("No. records", len(Sentiments.index))
        mlflow.log_metric("Accuracy", accuracy*100)
        mlflow.log_metric("Time", ending_time-starting_time)

        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme

        # Model registry does not work with file store
        if tracking_url_type_store != "file":

            # Register the model
            # There are other ways to use the Model Registry, which depends on the use case,
            # please refer to the doc for more information:
            # https://mlflow.org/docs/latest/model-registry.html#api-workflow
            mlflow.sklearn.log_model(lr, "model", registered_model_name="ElasticnetTweetSentimentModel")
        else:
            mlflow.sklearn.log_model(lr, "model")
