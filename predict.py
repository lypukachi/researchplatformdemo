import requests
import json
import pandas
import string
from sklearn.feature_extraction.text import TfidfVectorizer
# `sample_input` is a JSON-serialized pandas DataFrame with the `split` orientation
sample_input = {
    "columns": [
        "target",
        "ids",
        "date",
        "flag",
        "user",
        "text"
    ],
    "data": [
        [0, 1467810672,'Mon Apr 06 22:19:49 PDT 2009', 'NO_QUERY', 'scotthamilton', 'testing here']
    ]
}
vector = TfidfVectorizer(ngram_range=(1, 2))

#  # 1. read real data
#     # Get the dataset
Trump = pandas.read_csv('hashtag_donaldtrump.csv', lineterminator='\n')
Biden = pandas.read_csv('hashtag_joebiden.csv', lineterminator='\n')
   
Trump = Trump[['tweet','lat','long']]
Biden = Biden[['tweet','lat','long']]

#     # And let's clean our reviews
Trump['tweet'] = Trump['tweet'].dropna().apply(clear_sentence)
Biden['tweet'] = Biden['tweet'].dropna().apply(clear_sentence)

#     # A function to get only the data inside the USA (there are many tweets from abroad)
def get_region(data, bot_lat, top_lat, left_lon, right_lon):
        top = data.lat <= top_lat
        bot = data.lat >= bot_lat
        left = data.long >= left_lon
        right = data.long <= right_lon
        index = top&bot&left&right 
        return data[index]

Trump = get_region(Trump,24,50,-126,-65)
Biden = get_region(Biden,24,50,-126,-65)
    
#     print("Word cloud for Trump")
#     word_cloud_plot(Trump)
    
#     print("Word cloud for Biden")
#     word_cloud_plot(Biden)
    
#     # apply model to get predict sentiment analytic

#     trump_sentiment = pandas.DataFrame(model.predict(vector.transform(Trump['tweet'].tolist())),columns=['sentiment'])
#     biden_sentiment = pandas.DataFrame(model.predict(vector.transform(Biden['tweet'].tolist())),columns=['sentiment'])

#     Trump = pandas.concat([Trump.reset_index(drop=True), trump_sentiment], axis=1)
#     Biden = pandas.concat([Biden.reset_index(drop=True), biden_sentiment], axis=1)

#     trump_positive = Trump[Trump['sentiment'] == 'positive']
#     biden_positive = Biden[Biden['sentiment'] == 'positive']

#     Map = Basemap(llcrnrlat=24,urcrnrlat=50,llcrnrlon=-126,urcrnrlon=-65)
#     plt.figure(figsize=(12,10))
#     Map.bluemarble(alpha=0.6)

#     seaborn.scatterplot(x='long', y='lat', data=biden_positive, linewidth=0, s=40, alpha=1, label='Support for Biden')
#     seaborn.scatterplot(x='long', y='lat', data=trump_positive, linewidth=0, s=40, alpha=0.01, label='Support for Trump')

#     plt.gca().get_legend().legendHandles[1].set_alpha(1)
#     plt.title("Tweets positive towards presidential candidates")
#     plt.show()
temp = vector.transform(Trump['tweet'].tolist())

response = requests.post(
              url='http://127.0.0.1:5555/invocations', data=json.dumps(temp),
              headers={"Content-type": "application/json"})
response_json = json.loads(response.text)
print(response_json)

def clear_sentence(sentence: str) -> str:
    sentence = sentence.translate(str.maketrans(string.punctuation, ' ' * len(string.punctuation)))
    sentence = sentence.lower()
    return sentence

