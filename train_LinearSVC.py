# system
import os
import sys
    
# utilities
import warnings
import pandas
import string
import time

#models

from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC
from collections import Counter

#mlflow 
import mlflow
import mlflow.sklearn
import logging
from urllib.parse import urlparse
# mlflow logging
logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)
    
def clear_sentence(sentence: str) -> str:
    sentence = sentence.translate(str.maketrans(string.punctuation, ' ' * len(string.punctuation)))
    sentence = sentence.lower()
    return sentence

if __name__ == "__main__":
   
    warnings.filterwarnings("ignore")
    print('Loading train data')
    # Load the data and take a look at how tweet datasets usually look like
    Sentiments = pandas.read_csv('training.1600000.processed.noemoticon.csv', encoding="ISO-8859-1", names=["target", "ids", "date", "flag", "user", "text"])
    
    print(Sentiments.shape)
    Sentiments.head(10)
    Sentiments.info()
    Sentiments.describe()
    
      # Now, we only use text and the sentiment field for training. 
    Sentiments = Sentiments[['target','text']]
     
    
     # Make the sentiments strings
    sentiment_value = {0: "negative", 2: "neutral", 4: "positive"}
    decode = lambda label: sentiment_value[int(label)]
    print('clean data, please wait...')
    x = Sentiments['text'].apply(clear_sentence).tolist()
    y = Sentiments['target'].apply(decode).tolist()
    
#     print('Lets look at the distribution of tweets in the train set')
#     temp = Sentiments.groupby('target').count()['text'].reset_index().sort_values(by='text',ascending=False)
#     temp.style.background_gradient(cmap='Purples')
    
#     # plot the train data
#     plt.figure(figsize=(12,6))
#     seaborn.countplot(x='target',data=Sentiments)
    
   # Let's use the SVC model.
    starting_time = time.time()   
    vector = TfidfVectorizer(ngram_range=(1, 2))
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
    X_training = vector.fit_transform(X_train) 
    X_testing = vector.transform(X_test)
    
    # Useful for multiple runs (only doing one run in this sample notebook)    
    with mlflow.start_run():
        model = LinearSVC()
        print('train model...')
        model.fit(X_training, y_train)
        y_prediction = model.predict(X_testing)
        accuracy = accuracy_score(y_test, y_prediction)
        ending_time = time.time()
        print('Trained our model in',len(Sentiments.index),'tweets')
        print('Accuracy:',"{:.2f}".format(accuracy*100) + " in {:.2f}s".format(ending_time-starting_time))
        
        #logging metric to mlflow
        mlflow.log_metric("No. records", len(Sentiments.index))
        mlflow.log_metric("Accuracy", accuracy*100)
        mlflow.log_metric("Time", ending_time-starting_time)
        
        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme

        # Model registry does not work with file store
        if tracking_url_type_store != "file":

            # Register the model
            # There are other ways to use the Model Registry, which depends on the use case,
            # please refer to the doc for more information:
            # https://mlflow.org/docs/latest/model-registry.html#api-workflow
            mlflow.sklearn.log_model(model, "model", registered_model_name="demo-sentiment")
        else:
            mlflow.sklearn.log_model(model, "model")
